# Base Image, GNUstep dev environment
FROM mediabook/gnustep

MAINTAINER Scott Christley <schristley@mac.com>

# PROXY: uncomment these if building behind UTSW proxy
ENV http_proxy 'http://proxy.swmed.edu:3128/'
ENV https_proxy 'https://proxy.swmed.edu:3128/'
ENV HTTP_PROXY 'http://proxy.swmed.edu:3128/'
ENV HTTPS_PROXY 'https://proxy.swmed.edu:3128/'

# Install OS Dependencies
RUN DEBIAN_FRONTEND='noninteractive' apt-get update
RUN DEBIAN_FRONTEND='noninteractive' apt-get install -y \
    make \
    wget \
    emacs \
    cpanminus \
    libgd-dev \
    liblist-allutils-perl \
    ncbi-blast+ \
    bowtie bowtie2 \
    python3 \
    python3-dev \
    python3-sphinx \
    python3-pip \
    zlib1g-dev \
    cpio

RUN pip3 install \
    numpy \
    lxml \
    biopython \
    argparse \
    BeautifulSoup4 \
    reportlab

RUN mkdir /biotools

# circos
RUN cpanm Clone Config::General Font::TTF::Font GD GD::Polyline Math::Bezier Math::Round Math::VecStat
RUN cpanm Params::Validate Readonly Regexp::Common SVG Set::IntSpan Statistics::Basic Text::Format
RUN cd /biotools && wget http://circos.ca/distribution/circos-0.69-6.tgz
RUN cd /biotools && tar zxvf circos-0.69-6.tgz

ENV PATH "/biotools/circos-0.69-6/bin:$PATH"
